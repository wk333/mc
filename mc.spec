Name:		mc
Summary:	a feature rich full-screen text mode application
Version:	4.8.29
Release:	1
Epoch:		1
License:	GPLv3+
URL:		http://www.midnight-commander.org/
Source0:	https://github.com/MidnightCommander/mc/archive/%{version}.tar.gz

Patch0:		%{name}-spec.syntax.patch
Patch1:		%{name}-python3.patch
Patch2:		%{name}-default_setup.patch
Patch3:		%{name}-tmpdir.patch

BuildRequires:  gcc e2fsprogs-devel glib2-devel gpm-devel groff-base
BuildRequires:  libssh2-devel >= 1.2.5 perl-generators pkgconfig slang-devel
BuildRequires:  make autoconf automake gettext-devel libtool python3-boto3

%description
GNU Midnight Commander is a visual file manager, licensed under GNU General Public
License and therefore qualifies as Free Software. It's a feature rich full-screen
text mode application that allows you to copy, move and delete files and whole
directory trees, search for files and run commands in the subshell. Internal viewer
and editor are included.

Midnight Commander is based on versatile text interfaces, such as Ncurses or S-Lang,
which allows it to work on a regular console, inside an X Window terminal, over SSH
connections and all kinds of remote shells.

%package python
Summary:	Midnight Commander s3+ and UC1541 EXTFS backend scripts
BuildArch:	noarch
Requires:	%{name} = %{epoch}:%{version}-%{release}
Requires:	python3-boto3

%description python
Midnight Commander s3+ and UC1541 EXTFS backend scripts.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
sed -i "s,PREV_MC_VERSION=\"unknown\",PREV_MC_VERSION=\"%{version}\"," version.sh
./autogen.sh
%configure \
        PYTHON=%{__python3} \
	CFLAGS="%{optflags} -Wno-strict-aliasing" \
	--enable-vfs-sfs \
	--disable-vfs-sftp \
	--disable-vfs-smb \
	--enable-vfs-ftp \
	--enable-charset \
	--enable-largefile \
	--enable-vfs-cpio \
	--enable-vfs-extfs \
	--enable-vfs-fish \
	--enable-vfs-tar \
	--disable-rpath \
	--with-x \
	--with-gpm-mouse \
	--with-screen=slang \
	%{nil}
%make_build

%install
mkdir -p  ${RPM_BUILD_ROOT}%{_sysconfdir}/profile.d

%make_install

install contrib/mc.{sh,csh} ${RPM_BUILD_ROOT}%{_sysconfdir}/profile.d

%find_lang %{name} --with-man

%files -f %{name}.lang
/etc/profile.d/*
%dir %{_sysconfdir}/%{name}
%{_sysconfdir}/%{name}/edit*
%config(noreplace) %{_sysconfdir}/%{name}/*
%{_bindir}/*
%dir %{_libexecdir}/%{name}
%attr(755,root,root) %{_libexecdir}/%{name}/cons.saver
%{_libexecdir}/%{name}/*
%{_datadir}/%{name}
%exclude %{_libexecdir}/mc/extfs.d/{s3+,uc1541}

%files python
%{_libexecdir}/mc/extfs.d/{s3+,uc1541}

%files help
%{_mandir}/man1/*
%license doc/COPYING
%doc doc/FAQ doc/NEWS doc/README

%changelog
* Wed Apr 19 2023 wangkai <13474090681@163.com> - 1:4.8.29-1
- Update to 4.8.29

* Wed Jul 20 2022 yaoxin <yaoxin30@h-partners.com> - 1:4.8.28-1
- Update to 4.8.28 to fix CVE-2021-36370

* Tue Nov 19 2019 caomeng<caomeng5@huawei.com> - 1:4.8.21-3
- Package init
